/**
 * Webgame Module
 *
 * Provides basic API for work with Webgame.cz webpage.
 * This module extending CasperJS functionality via Casper-Galaxy project.
 */

/**
 * Create instance of Webgame module
 *
 * @param {CasperGalaxy} context
 */
var WebgameModule = function(context) {
    this.c = context;
};


/**
 * Load page with Webgame.cz login form and submit credentials
 *
 * @param  {string} username
 * @param  {string} password
 * @param  {string} url
 */
WebgameModule.prototype.login = function(username, password, url) {
    if (!url) {
        url = 'https://www.webgame.cz/';
    }

    this.c.log("Opening Webgame sign-in page: " + url, "debug");

    this.c.open(url).then(function() {
        this.waitForSelector('#login_form', function() {
            this.log("Login form selector found.", "info");

            if (!(typeof username === 'string' && typeof password === 'string')) {
                this.log("WebgameModule.fillLoginForm(): Fill login and password please.", "error");
            }

            if (this.exists('#login_form')) {
                this.log("WebgameModule.submitLogin(): Login form found.", "info");
            } else {
                this.log("WebgameModule.submitLogin(): Login form not found.", "error");
                return false;
            }

            this.fillSelectors('#login_form', {
                'input[name="email"]': username,
            });

            this.fillSelectors('#login_form', {
                'input[name="pass"]': password,
            });

            this.wait(500, function() {
                this.click('#loginbutton');
                // @todo Random wait?
                this.wait(2000, function() {
                    var url = this.getCurrentUrl();
                    if (~url.indexOf('login.php') || ~url.indexOf('login_attempt')) {
                        this.log('WebgameModule.login(): failed (URL: "' + url + '").', 'error');
                        this.capture("login-fail.png");
                        return false;
                    } else if (this.exists('#logoutMenu')) {
                        this.log("WebgameModule.login(): successfully logged in.", "info");
                    } else {
                        this.log("WebgameModule.login(): unknown error in Webgame.cz login process.", "debug");
                    }

                    return true;
                });
            });
        });
    });
};




WebgameModule.prototype.sendMessageById = function wgSendMessageById(wgTargetId, message) {

};


exports.WebgameModule = WebgameModule;
